#!/bin/bash
if [[ "push" = "$1" ]]; then
    for BR in $(git branch|sed -rn 's/^\s+(patch-.*)$/\1/p'); do
        git push my $BR:$BR --force
    done
elif [[ "pull" = "$1" ]]; then
    for BR in $(git br -a | sed -nr 's/.*my\/(patch.*)/\1/p'); do
        git branch -D $BR;
        git checkout $BR;
    done
else
    echo "$0 [push|pull]"
    echo ""
    echo "Beware: it force deletes any local patches branches."
fi
